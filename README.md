## Blogs service

Blogs site REST web service based on DRF, JWT

Bot to populate and test the site


### Blogs Features

- Fake users, posts, and votes generations
- JWT authentification, authorization, permissions
- CRUD posts
- Vote up, vote down, unvote
- Markdown
- Dates (created, updated)
- Slugs
- Read time


### Bot Features

- Works with the help of requests http client
- Generates users' data and posts
- Registers users
- Obtains JWT tokens
- Publishes random number of posts
- Votes up or down random posts
- Logging info to CLI


### Not Implemented (but desired in future)

- Images, sumbnails
- Comments
- Likes to comments
- uuid field
- Throttling


## Quick start


### Server

```
$ git clone https://valram@bitbucket.org/valram/django_blogs_service.git
$ cd django_blogs_service
$ virtualenv --python=python3.4 .
$ source bin/activate
$ pip3 install -r requirements.txt
$ python blogs/manage.py makemigrations
$ python blogs/manage.py migrate
$ python blogs/manage.py createsuperuser
$ python blogs/manage.py runserver

Performing system checks...

System check identified no issues (0 silenced).
January 22, 2018 - 21:06:36
Django version 2.0.1, using settings 'blogs.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

```



### Bot client


```
$ cd ~/path/to/django_blogs_service
$ source bin/activate
$ export INFO=1 # for logging info to cli
$ export API_VERSION=api_v1 # by default API_VERSION=api_v1
$ python blogs_bot.py --json_file bot_config_json_file  #  default is bot_cfg.json

```



The code is written for educational purposes.
