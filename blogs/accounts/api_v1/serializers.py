from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.db.models import Q


from rest_framework.serializers import (
    CharField,
    EmailField,
    ModelSerializer,
)


User = get_user_model()


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
        ]


class UserCreateSerializer(ModelSerializer):
    email = EmailField(label='Email address')
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'password',
        ]
        extra_kwargs = {'password':
                            {'write_only': True}
                        }

    def validate_email(self, value):
        email = self.get_initial().get('email')
        user_qs = User.objects.filter(email=email)
        if user_qs:
            raise ValidationError('User with this email already exists')
        return email

    def validate_username(self, value):
        username = self.get_initial().get('username')
        user_qs = User.objects.filter(username=username)
        if user_qs:
            raise ValidationError('User with this name already exists')
        return username

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        new_user = User(username=username, email=email)
        new_user.set_password(password)
        new_user.save()
        return validated_data


class UserLoginSerializer(ModelSerializer):
    username = CharField(label='User name', required=False, allow_blank=True)
    email = EmailField(label='Email address', required=False, allow_blank=True)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]
        extra_kwargs = {'password': {'write_only': True},}

    def validate(self, data):
        username = data.get('username', None)
        email = data.get('email', None)
        password = data.get('password', None)
        if not any((username, email)):
            raise ValidationError(
                'Username or email  must be provided')
        user_obj = User.objects.filter(
            Q(email=email) |
            Q(username=username)
        ).distinct().first()
        if not user_obj:
            raise ValidationError('This username or email is not valid.')
        if not user_obj.check_password(password):
            raise ValidationError('Incorrect credentials. Please try again.')
        return data
