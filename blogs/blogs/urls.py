from django.conf.urls import include, url
from django.contrib import admin


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


urlpatterns = [
    url(r'^api/v1/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair-v1'),
    url(r'^api/v1/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh-v1'),
    url(r'^api/v1/token/verify/$', TokenVerifyView.as_view(), name='token_verify-v1'),

    url(r'^api/v1/posts/', include('posts.api_v1.urls'), name='posts-api-v1'),

    url(r'^api/v1/accounts/', include('accounts.api_v1.urls'), name='accounts-api-v1' ),

    url(r'^admin/', admin.site.urls),
]
