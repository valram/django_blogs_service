from django.contrib import admin


from .models import Post


class PostModelAdmin(admin.ModelAdmin):
    list_display = ['user', 'title', 'votes_count', 'slug',
                    'content', 'updated',]
    list_filter = ['user', 'updated']
    search_fields = ['user', 'title', 'content',]
    class Meta:
        model = Post


admin.site.register(Post, PostModelAdmin)
