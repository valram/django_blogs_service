from django.contrib.auth import get_user_model


from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    PrimaryKeyRelatedField,
)


from accounts.api_v1.serializers import UserDetailSerializer
from posts.models import Post


User = get_user_model()


post_delete_url = HyperlinkedIdentityField(
    view_name='delete',
    lookup_field='slug',
)

post_detail_url = HyperlinkedIdentityField(
    view_name='detail',
    lookup_field='slug',
)

post_vote_up_url = HyperlinkedIdentityField(
    view_name='vote_up',
    lookup_field='slug',
)

post_vote_down_url = HyperlinkedIdentityField(
    view_name='vote_down',
    lookup_field='slug',
)

post_vote_delete_url = HyperlinkedIdentityField(
    view_name='vote_delete',
    lookup_field='slug',
)


class PostCreateSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    url = post_detail_url
    class Meta:
        model = Post
        fields = [
            'user',
            'title',
            'slug',
            'url',
            'content',
            'votes_count',
        ]


class PostUpdateSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    url = post_detail_url
    class Meta:
        model = Post
        fields = [
            'user',
            'title',
            'url',
            'content',
            'votes_count',
        ]


class PostDetailSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    html = SerializerMethodField()
    delete_url = post_delete_url
    vote_up_url = post_vote_up_url
    vote_down_url = post_vote_down_url
    vote_delete_url = post_vote_delete_url

    class Meta:
        model = Post
        fields = [
            'id',
            'user',
            'title',
            'slug',
            'content',
            'html',
            'votes_count',
            'delete_url',
            'vote_up_url',
            'vote_down_url',
            'vote_delete_url',
        ]

    def get_html(self, obj):
        return obj.get_markdown()


class PostListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    url = post_detail_url
    truncated_content = SerializerMethodField()
    delete_url = post_delete_url
    class Meta:
        model = Post
        fields = [
            'id',
            'slug',
            'url',
            'user',
            'title',
            'votes_count',
            'truncated_content',
            'delete_url',
        ]

    def get_truncated_content(self, obj, max_length=300):
        return (
            obj.content[:max_length]
            if len(obj.content) > max_length
            else obj.content
        )

class PostVoteSerializer(ModelSerializer):
    url = post_detail_url

    class Meta:
        model = Post
        fields = [
            'url',
            'votes_count',
        ]
