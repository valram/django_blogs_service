from django.conf.urls import url

from .views import (
    PostCreateAPIView,
    PostUpdateAPIView,
    PostDeleteAPIView,
    PostDetailAPIView,
    PostListAPIView,

    PostVoteUpAPIView,
    PostVoteDownAPIView,
    PostVoteDeleteAPIView,
)

urlpatterns = [
    url(r'^$', PostListAPIView.as_view(), name='list'),
    url(r'^create/$', PostCreateAPIView.as_view(), name='create'),
    url(r'^(?P<slug>[\w-]+)/$', PostDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/delete/$', PostDeleteAPIView.as_view(), name='delete'),
    url(r'^(?P<slug>[\w-]+)/update/$', PostUpdateAPIView.as_view(), name='update'),

    url(r'^(?P<slug>[\w-]+)/vote/up/$', PostVoteUpAPIView.as_view(), name='vote_up'),
    url(r'^(?P<slug>[\w-]+)/vote/down/$', PostVoteDownAPIView.as_view(), name='vote_down'),
    url(r'^(?P<slug>[\w-]+)/vote/delete/$', PostVoteDeleteAPIView.as_view(), name='vote_delete'),
]

