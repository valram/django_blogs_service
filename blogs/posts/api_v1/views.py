from django.db.models import Q


from rest_framework.generics import (
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    CreateAPIView,
    get_object_or_404,
)
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.permissions import (
    AllowAny,
    IsAdminUser,
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
)


from posts.models import Post
from .permissions import IsOwnerOrReadOnly
from .serializers import (
    PostListSerializer,
    PostDetailSerializer,
    PostCreateSerializer,
    PostUpdateSerializer,
    PostVoteSerializer,
)


class PostCreateAPIView(CreateAPIView):
    serializer_class = PostCreateSerializer
    queryset = Post.objects.all()
    lookup_field = 'slug'
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PostUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostUpdateSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    lookup_field = 'slug'

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    lookup_field = 'slug'


class PostDetailAPIView(RetrieveAPIView):
    serializer_class = PostDetailSerializer
    queryset = Post.objects.all()
    lookup_field = 'slug'
    permission_classes = (AllowAny,)


class PostListAPIView(ListAPIView):
    serializer_class = PostListSerializer
    filter_backend = (SearchFilter,)
    search_fields = ('title', 'content',
                     'user__first_name', 'user__last_name',)
    permission_classes = (AllowAny,)

    def get_queryset(self, *args, **kwargs):
        queryset_list = Post.objects.all()
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query)|
                Q(content__icontains=query)|
                Q(user__first_name__icontains=query)|
                Q(user__last_name__icontains=query)
            ).distinct()
        return queryset_list


class PostVoteAPIView(RetrieveUpdateAPIView):
    serializer_class = PostVoteSerializer
    queryset = Post.objects.all()
    permission_classes = (IsAuthenticated,)
    lookup_field = 'slug'


class PostVoteUpAPIView(PostVoteAPIView):

    def put(self, request, *args, **kwargs):
        post_obj = get_object_or_404(
            self.get_queryset(),
            slug=kwargs.get('slug')
        )
        post_obj.votes.up(user_id=request.user.pk)
        return self.update(request, *args, **kwargs)


class PostVoteDownAPIView(PostVoteAPIView):

    def put(self, request, *args, **kwargs):
        post_obj = get_object_or_404(
            self.get_queryset(),
            slug=kwargs.get('slug')
        )
        post_obj.votes.down(user_id=request.user.pk)
        return self.update(request, *args, **kwargs)


class PostVoteDeleteAPIView(PostVoteAPIView):

    def put(self, request, *args, **kwargs):
        post_obj = get_object_or_404(
            self.get_queryset(),
            slug=kwargs.get('slug')
        )
        post_obj.votes.delete(user_id=request.user.pk)
        return self.update(request, *args, **kwargs)
