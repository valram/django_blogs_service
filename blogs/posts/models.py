from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.utils.safestring import mark_safe
from django.utils import timezone
from django.db.models.signals import pre_save


from markdown_deux import markdown
from vote.models import VoteModel


from .utils import get_read_time


class Post(VoteModel, models.Model):
    title = models.CharField(max_length=120, db_index=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,)
    slug = models.SlugField(unique=True, blank=True, null=True, db_index=True)
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    read_time = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    def get_api_url(self):
        return reverse('posts-api-v1:detail', kwargs={'slug': self.slug})

    def get_markdown(self):
        return mark_safe(markdown(self.content))

    @property
    def votes_count(self):
        return self.votes.count()

    class Meta:
        db_table = 'posts'
        ordering = ['-created', '-updated']


def create_slug(instance, new_slug=None):
    if new_slug:
        slug = new_slug
    else:
        slug = slugify(instance.title, allow_unicode=True)
    query_set = Post.objects.filter(slug=slug).order_by('-id')
    if query_set.exists():
        new_slug = '{1}-{2}'.format(slug, query_set.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)
    if instance.content:
        html_text = instance.get_markdown()
        instance.read_time = get_read_time(html_text)


pre_save.connect(pre_save_post_receiver, sender=Post)
