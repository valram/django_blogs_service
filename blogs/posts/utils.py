import re
import math


from django.utils.html import strip_tags


def count_words(html_string):
    return len(re.findall(r'\w+', strip_tags(html_string)))


def get_read_time(html_string):
    count = count_words(html_string)
    return int(math.ceil(count / 150)) # assuming 150 words/min
