import argparse
import logging
from os import getenv
import re
import sys
import time
import json
from random import choice, randint, sample


import barnum
import requests
from requests.exceptions import RequestException


if getenv('INFO'):
    logging.basicConfig(level=logging.INFO, format='%(message)s',)


API_VERSION = getenv('API_VERSION', 'api_v1')


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-j', '--json_file',
        type=argparse.FileType(mode='r'),
        default='bot_cfg.json',
        help='path to the configuration file in json format '
    )
    return parser


def obtain_cli_args(start=1):
    return create_parser().parse_args(sys.argv[start:])


def read_args_from_json_file(json_file_handler):
    with json_file_handler:
        return json.load(json_file_handler)


def populate_site(config, start=1, timeout=0.5):
    api = config[API_VERSION]
    users_tokens, posts_slugs = [], []
    for number in range(config['number_of_users']):
        with requests.Session() as session:
            username, passw = register_new_user(config, api, session)
            time.sleep(timeout)
            tokens = obtain_jwt_tokens(config, api, session, username, passw)
            users_tokens.append(tokens)
            time.sleep(timeout)
            posts_slugs.extend(
                generate_rand_posts_per_user(config, api, session, tokens))
            time.sleep(timeout)
    vote_rand_posts_by_each_user(config, api, users_tokens, posts_slugs)


def register_new_user(config, api, session):
    username = barnum.create_name(full_name=False)
    email = barnum.create_email()
    passw = barnum.create_pw()
    register_payload = {
        "username": username,
        "email": email,
        "password": passw,
    }
    session.headers.update({'Content-Type': 'application/json',})
    register_resp = session.post(
        config['blogs_url'].format(path=api['register_path']),
        data=json.dumps(register_payload),
    )
    logging.info(
        '\nRegistered new user: {name}'.format(
            name=json.loads(register_resp.text)['username']))
    return username, passw



def obtain_jwt_tokens(config, api, session, username, passw):
    jwt_resp = session.post(
        config['blogs_url'].format(path=api['obtain_token']),
        data=json.dumps({
            "username": username,
            "password": passw,
        }),
    )
    logging.info(
        'Access token: {token}'.format(
            token=json.loads(jwt_resp.text)['access']))
    return json.loads(jwt_resp.text)


def generate_rand_posts_per_user(
        config, api, session, tokens, start=1,  timeout=0.5,
        max_title_lenght=3, min_content_lenght=20, max_content_lenght=100,):
    posts_slugs = []
    session.headers.update({'Authorization':
                    'Bearer {token}'.format(token=tokens['access']),})
    for number in range(randint(start, config['max_posts_per_user'])):
        post_create_resp = session.post(
            config['blogs_url'].format(path=api['post_create']),
            data=json.dumps({
                'title': barnum.create_nouns(
                    max=max_title_lenght),
                'content': barnum.create_sentence(
                    min=min_content_lenght,
                    max=max_content_lenght),
            },)
        )
        posts_slugs.append(json.loads(post_create_resp.text)['slug'])
        time.sleep(timeout)
        logging.info('New post: {title}'.format(
            title=json.loads(post_create_resp.text)['title']))
    return posts_slugs


def vote_rand_posts_by_each_user(
    config, api, users_tokens, posts_slugs, timeout=0.5):
    logging.info('\nStarts vote_rand_posts_by_each_user()\n')
    for tokens in users_tokens:
        with requests.Session() as session:
            session.headers.update({'Authorization':
                'Bearer {token}'.format(token=tokens['access']),
                'Content-Type': 'application/json',
            })
            if config['max_votes_per_user'] >= len(posts_slugs):
                rand_slugs = posts_slugs # we can't vote post twice
            else:
                rand_slugs = sample(
                    posts_slugs, config['max_votes_per_user']
                )
            for slug in rand_slugs:
                vote_path = choice((
                    api['post_vote_up'],
                    api['post_vote_down'])).format(slug=slug)
                vote_resp = session.put(
                    config['blogs_url'].format(path=vote_path)
                )
                logging.info(
                    '{slug:<30} {vote_choice:<15}: {votes_count:<5}'.format(
                        slug=slug.upper(),
                        vote_choice=get_vote_choice_from_path(vote_path),
                        votes_count=json.loads(vote_resp.text)['votes_count'],)
                )
                time.sleep(timeout)


def get_vote_choice_from_path(vote_path):
    if 'up' in vote_path:
        return 'voted up'
    elif 'down' in vote_path:
        return 'voted down'
    else:
        return 'vote deleted'


def output_posts_json_to_cli(url='http://127.0.0.1:8000/api/v1/posts/'):
    posts_resp = requests.get(url)
    print(
        json.dumps(
            json.loads(posts_resp.text),
            indent=4,
            sort_keys=True,
            ensure_ascii=False,
        )
    )


if __name__ == '__main__':
    config = read_args_from_json_file(obtain_cli_args().json_file)
    populate_site(config)
    output_posts_json_to_cli()
